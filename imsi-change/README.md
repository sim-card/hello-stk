# IMSI change SIM applet

Display and change the IMSI of the SIM. This is a standalone version of a debug
feature in the more complex IMSI Pseudonymization applet. To be used as example
code to build other applets.

### Related

* [IMSI Pseudonymization](https://osmocom.org/projects/imsi-pseudo/wiki)
