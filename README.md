﻿# SIM Toolkit Examples

This repository contains example Java SIM Toolkit applications.

Build requirements
------------------

### Install ANT:

ANT is required to control the build process. It can be installed via APT
(debian)

```
$ sudo apt install ant
```

### Download a suitable JDK:

For compatibility reasons it is recommended to download a specific JDK and use
it to compile the javacard applets. The code in this repository has been tested
with the following JDK variants:

Oracle Java SE Development Kit 11.0.24
<https://www.oracle.com/de/java/technologies/javase/jdk11-archive-downloads.html>

OpenJDK 11.0.2 (build 11.0.2+9)
<https://jdk.java.net/archive/>

The easiest method to install the JDK is to download the tar.gz version and to
unpack it to a suitable location and to let the environment variable `JAVA_HOME`
point to that location.

### Download JAVA-card SDKs:

There is a curated github repository that contains a good selection of JAVA-card
SDKs: <https://github.com/martinpaljak/oracle_javacard_sdks.git>

Use the following commandline to download the Javacard SDKs into the (empty)
directory oracle_javacard_sdks:

```
$ git submodule update --init --recursive
```

Building CAP files
------------------

The build process is started by running `ant`. The environment variable
`JAVA_HOME` must point to the location of the JDK.

ANT will also automatically download another required component called
"ant-javacard.jar". See <https://github.com/martinpaljak/ant-javacard> for
more information.

```
$ JAVA_HOME=/home/user/bin/jdk/oracle/jdk-11.0.24/ ant
Buildfile: /home/user/work/simcard_applet_loading/hello-stk_osmo/build.xml

dist:
      [get] Destination already exists (skipping): /home/user/work/simcard_applet_loading/hello-stk_osmo/ant-javacard.jar
      [cap] INFO: using JavaCard 3.0.5 SDK in /home/user/work/simcard_applet_loading/hello-stk_osmo/oracle_javacard_sdks/jc305u3_kit with JDK 11
      [cap] INFO: targeting JavaCard 2.2.1 SDK in /home/user/work/simcard_applet_loading/hello-stk_osmo/oracle_javacard_sdks/jc221_kit
      [cap] INFO: Setting package name to org.toorcamp.HelloSTK
      [cap] Building CAP with 1 applet from package org.toorcamp.HelloSTK (AID: D07002CA44)
      [cap] org.toorcamp.HelloSTK.HelloSTK D07002CA44900101
  [compile] Compiling files from /home/user/work/simcard_applet_loading/hello-stk_osmo/hello-stk
  [compile] Compiling 1 source file to /home/user/work/simcard_applet_loading/hello-stk_osmo/build/classes
  [compile] /home/user/work/simcard_applet_loading/hello-stk_osmo/hello-stk/src/org/toorcamp/HelloSTK/HelloSTK.java
   [verify] Verification passed
      [cap] CAP saved to /home/user/work/simcard_applet_loading/hello-stk_osmo/build/HelloSTK.cap
      [cap] INFO: using JavaCard 3.0.5 SDK in /home/user/work/simcard_applet_loading/hello-stk_osmo/oracle_java
      [cap] INFO: targeting JavaCard 2.2.1 SDK in /home/user/work/simcard_applet_loading/hello-stk_osmo/oracle_
      [cap] INFO: Setting package name to org.osmocom.IMSIChange
      [cap] Building CAP with 1 applet from package org.osmocom.IMSIChange (AID: D07002CA44)
      [cap] org.osmocom.IMSIChange.IMSIChange D07002CA44900102
  [compile] Compiling files from /home/user/work/simcard_applet_loading/hello-stk_osmo/imsi-change
  [compile] Compiling 3 source files to /home/user/work/simcard_applet_loading/hello-stk_osmo/build/classes
  [compile] /home/user/work/simcard_applet_loading/hello-stk_osmo/imsi-change/src/org/osmocom/IMSIChange/Bytes.
  [compile] /home/user/work/simcard_applet_loading/hello-stk_osmo/imsi-change/src/org/osmocom/IMSIChange/IMSICh
  [compile] /home/user/work/simcard_applet_loading/hello-stk_osmo/imsi-change/src/org/osmocom/IMSIChange/Mobile
  [compile] /home/user/work/simcard_applet_loading/hello-stk_osmo/imsi-change/src/org/osmocom/IMSIChange/IMSICh
  [compile]                     proHdlr.appendTLV((byte)(TAG_DEFAULT_TEXT), (byte)4, prefillVal, (short)0,
  [compile]                                       ^
  [compile] /home/user/work/simcard_applet_loading/hello-stk_osmo/imsi-change/src/org/osmocom/IMSIChange/IMSICh
  [compile]             gsmFile.select((short) SIMView.FID_DF_GSM);
  [compile]                            ^
  [compile] /home/user/work/simcard_applet_loading/hello-stk_osmo/imsi-change/src/org/osmocom/IMSIChange/IMSICh
  [compile]             gsmFile.select((short) SIMView.FID_EF_IMSI);
  [compile]                            ^
  [compile] /home/user/work/simcard_applet_loading/hello-stk_osmo/imsi-change/src/org/osmocom/IMSIChange/IMSICh
  [compile]             gsmFile.select((short) SIMView.FID_DF_GSM);
  [compile]                            ^
  [compile] /home/user/work/simcard_applet_loading/hello-stk_osmo/imsi-change/src/org/osmocom/IMSIChange/IMSICh
  [compile]             gsmFile.select((short) SIMView.FID_EF_IMSI);
  [compile]                            ^
  [compile] /home/user/work/simcard_applet_loading/hello-stk_osmo/imsi-change/src/org/osmocom/IMSIChange/IMSICh
  [compile]             proHdlr.init((byte)PRO_CMD_REFRESH, SIM_REFRESH_SIM_INIT_FULL_FILE_CHANGE, DEV_ID_ME);
  [compile]                          ^
  [compile] /home/user/work/simcard_applet_loading/hello-stk_osmo/imsi-change/src/org/osmocom/IMSIChange/Mobile
  [compile]                     byte nibble = bcd[(byte)nibble_i >> 1];
  [compile]                                       ^
  [compile] 7 warnings
   [verify] Verification passed
      [cap] CAP saved to /home/user/work/simcard_applet_loading/hello-stk_osmo/build/ImsiChange.cap

BUILD SUCCESSFUL
Total time: 2 seconds
```

In case a different JDK has to be chosen it is important to check if the JDK is
compatible with the JAVA-card SDK. Not all combinations will yield usable
results.

A table with compatible JDK / JAVA-card SDK combinations can be found here:
<https://github.com/martinpaljak/ant-javacard/wiki/JavaCard-SDK-and-JDK-version-compatibility>

Loading CAP files
-----------------

The loading of the CAP files is typically done using the tools from <https://javacard.pro/globalplatform/>.

For more information see:
<https://osmocom.org/projects/cellular-infrastructure/wiki/HelloSTK>
